import { Controller , Get } from '@nestjs/common';
import { UserDto } from '../dtos/user.dto';
import { ApiOkResponse } from '@nestjs/swagger';

@Controller('users')
export class UsersController {

    @Get()
    @ApiOkResponse({ type: UserDto, isArray: true })
    findAll(): UserDto[] {
      return [
        {name: 'form' },
        {name: 'maew'},
      ];
    }
}
